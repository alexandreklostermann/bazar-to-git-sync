#!/usr/bin/env python
import datetime
import os
import locale
import subprocess

locale.setlocale(locale.LC_TIME, 'pt_BR.utf8')



# p0 = subprocess.Popen(["ls", "-l"], stdout=subprocess.PIPE)
# p1 = subprocess.Popen(["ls", "-lh"], stdout=subprocess.PIPE)
# p0.wait()
# p1.wait()
# a = p0.communicate()
# print a
# a = p1.communicate()
# print a


caminho = '/home/basesync/openerp_clone/'
server = caminho + 'server/'
web = caminho + 'web/'
addons = caminho + 'addons/'

log_file = '/home/basesync/sync/log.log'
log = open(log_file, 'a')


def log_time():
    return datetime.datetime.now().strftime('%d-%m-%Y %H:%M:%S: ')

def executa_comando_com_log(comando):
	processo = subprocess.Popen(comando, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
	processo.wait()

def bzr_update(path):
    os.chdir(path)
    print >> log, log_time() + '-> bazar: ' + path + ' fazendo pull'
    executa_comando_com_log(['bzr merge --force --pull'])
    #os.system('bzr merge --force --pull')
    print >> log, log_time() + '-> bazar: ' + path + ' pull finalizado'
    print >> log, log_time() + '-> bazar: ' + path + ' iniciando update'
    executa_comando_com_log(['bzr update'])
    #os.system('bzr update')
    print >> log, log_time() + '-> bazar: ' + path + ' update finalizado'

def git_update():
	os.chdir(caminho)
	print >> log, log_time() + '-> git: adicionando alteracoes ao git'
	executa_comando_com_log(['git add -A'])
	#os.system('git add -A')
	print >> log, log_time() + '-> git: alteracoes efetuadas'
	print >> log, log_time() + '-> git: fazendo commit local'
	executa_comando_com_log(['git commit -m "Merge do upstream do repositorio Bazar para git (automatico)"'])
	#os.system('git commit -m "Merge do upstream do repositorio Bazar para git (automatico)"')
	print >> log, log_time() + '-> git: commit local finalizado'
	print >> log, log_time() + '-> git: iniciando git push'
	executa_comando_com_log(['git push'])
	#os.system('git push')
	print >> log, log_time() + '-> git: git push finalizado'

print >> log, log_time() + '--------- Iniciando Sincronizacao --------------'
print >> log, log_time() + '-> acessando base Bazar do Lounchpad'
bzr_update(server)
bzr_update(web)
bzr_update(addons)
git_update()

print >> log, log_time() + '-------- Repositorios atualizados com sucesso ------'
